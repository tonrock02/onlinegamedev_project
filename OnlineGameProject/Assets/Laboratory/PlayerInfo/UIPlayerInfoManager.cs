﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class UIPlayerInfoManager : MonoBehaviour
{
    public Slider _HealthBar;
    public Text _TextNickName;

    PunHealth _userHealth;

    public void Awake() {
        _userHealth = GetComponentInParent<PunHealth>();
    }

    public void SetLocalUI()
    {
        //UI Control
        GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
        GetComponent<UIDirectionControl>().enabled = false;
    }

    public void SetNickName(string name)
    {
        if (_TextNickName != null)
            _TextNickName.text = name;
    }

    private void FixedUpdate()
    {
        if (_HealthBar != null && _userHealth != null)
        {
            int currentHealth = _userHealth.currentHealth;
            _HealthBar.value = (float)currentHealth / (float)100;
        }
    }
}
