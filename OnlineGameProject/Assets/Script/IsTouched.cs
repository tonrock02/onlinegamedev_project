﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class IsTouched : MonoBehaviourPun
{
    public int isTouched = 1;
    public int countAliveBlue;
    public void OnTriggerEnter(Collider other)
    {
        PunUserNetControl WhatTeam = this.GetComponent<PunUserNetControl>();
        if (other.gameObject.CompareTag("Player") && other.GetComponent<PunUserNetControl>().MyTeamIs.Name == "Blue")
        {
            PhotonView netview = other.GetComponent<PhotonView>();
            if (netview.ViewID != this.photonView.ViewID)
            {
                PlayerAttribute touched = other.gameObject.GetComponent<PlayerAttribute>();
                if (touched != null)
                {
                    if (!touched.isdie)
                    {
                        touched.TakeDamage(isTouched,photonView.ViewID);
                        Debug.Log("I got you!");
                    
                        //if(photonView.IsMine)
                        
                        //SettingHide(PhotonNetwork.CurrentRoom.CustomProperties);
                        PunNetworkManager.singleton.PlayerCounter--;
                        touched.isdie = true;
                        
                        Debug.Log("player minus :"+PunNetworkManager.singleton.PlayerCounter);
                    }
                    

                }
            }
        }
        
    }

    public void SettingHide(Hashtable propertiesThatChanged)
    {
        //write new
        
        object startTimeFromProps;

        if (propertiesThatChanged.TryGetValue(PunGameSetting.COUNTHIDENTEAM, out startTimeFromProps)) {
            
            //isTimerRunning = true;
            countAliveBlue = (int)startTimeFromProps;
            countAliveBlue--;
            Debug.Log("GetStartTime Prop is : " + countAliveBlue);
            Hashtable props = new Hashtable
            {
                {PunGameSetting.COUNTHIDENTEAM, countAliveBlue}
            };
            PhotonNetwork.CurrentRoom.SetCustomProperties(props);
        }
    }
}
