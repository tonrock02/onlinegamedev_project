﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;
using Random = UnityEngine.Random;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerAttribute : MonoBehaviourPunCallbacks
{
    public GameObject touchArea;

    public const int IsTouch = 1;

    public int amTouched = IsTouch;

    public bool Hider;

    public bool Seeker;
    
    public Text CooldownText;

    public GameObject CooldownCanvas;

    public bool isPreparePhase = true;
    public bool isGamePlayPhase = false;
    public bool isGameoverPhase = false;

    public PunGameTimer m_pungammtimer;
    public int m_istouchedd;
    public int CheckAlivePlayer;

    public bool isdie = false;

    public bool isClick = false;
    //public float TimeCountPraparePhase;

    [Header("Hider Attribute")]
    [SerializeField]
    public GameObject HiderLight;
    public GameObject HiderLightPreparePhase;
    public GameObject HiderLightDead;
    
    [Header("Seeker Attribute")]
    [SerializeField]
    public float SeekerSkillMoveSpeed;
    public float SeekerSkillCooldownTime;
    public float TouchCooldown;
    public bool skillcheck;
    public GameObject SeekerLight;
    public GameObject SeekerLightPreparePhase;

    [Header("ETC.")]
    public int amountRedteam;
    public int amountBlueTeam;
    public GameObject WinPanel;
    public GameObject LosePanel;
    public int CheckHiderAlive;

    // Start is called before the first frame update
    void Start()
    {
        m_pungammtimer = GameObject.FindObjectOfType<PunGameTimer>();
        //PunUserNetControl WhatTeam = this.GetComponent<PunUserNetControl>();
        if (this.GetComponent<PunUserNetControl>().MyTeamIs.Name == "Red")
        {
            Seeker = true;
            Hider = false;
            //photonView.RPC("CountRedTeam", RpcTarget.All);
            Debug.Log("I AM SEEKER");
        }
        if (this.GetComponent<PunUserNetControl>().MyTeamIs.Name == "Blue")
        {
            Hider = true;
            Seeker = false;
            //photonView.RPC("CountBlueTeam", RpcTarget.All);
            CooldownText.text = "";
            Debug.Log("I AM HIDER");
        }
        
        CheckAlivePlayer = PhotonNetwork.CurrentRoom.PlayerCount - 1;
        Debug.Log(CheckAlivePlayer);
        //isPreparePhase = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.IsMine)
            return;
        // if (Input.GetKeyDown(KeyCode.K))
        // {
        //     Hider = false;
        //     Seeker = true;
        //     Debug.Log("I AM SEEKER");
        // }
        //
        // if (Input.GetKeyDown(KeyCode.L))
        // {
        //     Seeker = false;
        //     Hider = true;
        //     Debug.Log("I AM HIDER");
        // }
        //if (PhotonNetwork.IsMasterClient)
        //{
            if (m_pungammtimer.currentCountDown >= 120f)
            {
                photonView.RPC("PunRPCPreparePhase", RpcTarget.All);
                Debug.Log("PreparePhase");
            }
        
            if (m_pungammtimer.currentCountDown <= 120f && m_pungammtimer.currentCountDown > 0f)
            {
                photonView.RPC("PunRPCGameplayPhase", RpcTarget.All);
                Debug.Log("GameplayPhase");
            }
        
            if (m_pungammtimer.currentCountDown < 0f)
            {
                photonView.RPC("PunRPCGameoverPhase", RpcTarget.All);
                Debug.Log("GameOverPhase");
            }
        //}



        if (isPreparePhase)
        {
            if (Seeker == true && Hider == false)
            {
                IsSeekerPreparePhase();
            }
        
            if (Hider == true && Seeker == false)
            {
                IsHiderPreparePhase();
            }
        }
        else if (isGamePlayPhase)
        {
            if (Seeker == true && Hider == false)
            {
                IsSeeker();
            }
        
            if (Hider == true && Seeker == false)
            {
                IsHider();
            }
        }
        else if (isGameoverPhase)
        {
            if (PunNetworkManager.singleton.PlayerCounter > 1 && Seeker == true)
            {
                //WinPanel.SetActive(false);
                LosePanel.SetActive(true);
            }
        
            else if (PunNetworkManager.singleton.PlayerCounter > 1 && Hider == true)
            {
                Debug.Log("Count");
                WinPanel.SetActive(true);
                //LosePanel.SetActive(false);
            }
        }

        if (Seeker == true)
        {
            if (SeekerSkillCooldownTime > 0)
            {
                SeekerSkillCooldownTime -= Time.deltaTime;
                CooldownText.text = "Cooldown :" + SeekerSkillCooldownTime.ToString("0.##");
            }

            if (skillcheck)
            {
                this.GetComponent<PlayerController>().movementSpeed = SeekerSkillMoveSpeed;
            }

            if (SeekerSkillCooldownTime <= 10f)
            {
                this.GetComponent<PlayerController>().movementSpeed = 10f;
                skillcheck = false;
            }

            if (SeekerSkillCooldownTime <= 0)
            {
                CooldownText.text = "Cooldown : Ready";
            }
        }

        // if (TimeCountPraparePhase > 0)
        // {
        //     TimeCountPraparePhase -= Time.deltaTime;
        // }
        //
        // if (TimeCountPraparePhase <= 0)
        // {
        //     isPreparePhase = false;
        //     isGamePlayPhase = true;
        // }

        if (PunNetworkManager.singleton.PlayerCounter <= 1 && Seeker == true )
        {
            //Debug.Log("Do");
            WinPanel.SetActive(true);
            //LosePanel.SetActive(false);
        }
        
        else if (PunNetworkManager.singleton.PlayerCounter <= 1 && Hider == true)
        {
            Debug.Log("Do2");
            //WinPanel.SetActive(false);
            LosePanel.SetActive(true);
        }
        //Debug.Log(CheckAlivePlayer);
        //Debug.Log(TouchCooldown);
        //Debug.Log(SeekerSkillCooldownTime);
        //CheckHider(PhotonNetwork.CurrentRoom.CustomProperties);
        //Debug.Log("is : " + CheckHiderAlive);
        //Debug.Log(touchArea.GetComponent<IsTouched>().countAliveBlue);
    }
    
    public void TakeDamage(int amount, int OwnerNetID)
    {
        if (photonView != null)
            photonView.RPC("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        else print("photonView is NULL.");
    }

    [PunRPC]
    public void PunRPCPreparePhase()
    {
        isPreparePhase = true;
        isGamePlayPhase = false;
        isGameoverPhase = false;
        Debug.Log("PunPrepare");
    }
    
    [PunRPC]
    public void PunRPCGameplayPhase()
    {
        isPreparePhase = false;
        isGamePlayPhase = true;
        isGameoverPhase = false;
        Debug.Log("PunGameplay");
    }
    
    [PunRPC]
    public void PunRPCGameoverPhase()
    {
        isPreparePhase = false;
        isGamePlayPhase = false;
        isGameoverPhase = true;
        Debug.Log("PunGameOver");
    }
    
    [PunRPC]
    public void PunRPCTakedDamage(int amount, int OwnerNetID)
    {
        Debug.Log("Touched");
        amTouched -= amount;
        if (amTouched <= 0)
        {
            Debug.Log("NetID : " + OwnerNetID.ToString() + " Killed " + photonView.ViewID);
            photonView.RPC("PunDeadPlayer", photonView.Owner);
        }
    }
    
    [PunRPC]
    public void ActiveTouch()
    {
        touchArea.GetComponent<BoxCollider>().enabled = true;
        //touchArea.GetComponent<MeshRenderer>().enabled = true;
    }
    
    [PunRPC]
    public void DeactiveTouch()
    {
        touchArea.GetComponent<BoxCollider>().enabled = false;
        //touchArea.GetComponent<MeshRenderer>().enabled = false;
    }

    // [PunRPC]
    // public void CountRedTeam()
    // {
    //     amountRedteam += 1;
    // }
    //
    // [PunRPC]
    // public void CountBlueTeam()
    // {
    //     amountBlueTeam += 1;
    // }
    
    [PunRPC]
    public void PunDeadPlayer()
    {
        Debug.Log("Dead....");
        // this.transform.position = new Vector3(Random.Range(-5.0f, 5.0f),
        //     10,
        //     Random.Range(-5.0f, 5.0f));
        //amTouched = IsTouch;
        HiderLightDead.SetActive(true);
        photonView.RPC("IsDead",RpcTarget.All);
    }


    public void IsHider()
    {
        SeekerLight.SetActive(false);
        SeekerLightPreparePhase.SetActive(false);
        HiderLight.SetActive(true);
        HiderLightPreparePhase.SetActive(false);
    }

    public void IsSeeker()
    {
        SeekerLight.SetActive(true);
        SeekerLightPreparePhase.SetActive(false);
        HiderLight.SetActive(false);
        HiderLightPreparePhase.SetActive(false);

        this.GetComponent<PlayerController>().enabled = true;
        if (Input.GetMouseButtonDown(0) && isClick == false)
        {
            Debug.Log("Yee");
            photonView.RPC("ActiveTouch", RpcTarget.All);
            //StartCoroutine(Cooldown());
            isClick = true;
            //photonView.RPC("DeactiveTouch", RpcTarget.All);
        }
        if (Input.GetMouseButtonUp(0) && isClick == true)
        {
            photonView.RPC("DeactiveTouch", RpcTarget.All);
            isClick = false;
        }
        
        if (Input.GetKeyDown(KeyCode.R) && SeekerSkillCooldownTime <= 0)
        {
            SeekerSkillCooldownTime = 15f;
            skillcheck = true;
        }
    }

    public void IsHiderPreparePhase()
    {
        //SeekerLight.SetActive(false);
        //SeekerLightPreparePhase.SetActive(false);
        HiderLight.SetActive(false);
        HiderLightPreparePhase.SetActive(true);
    }
    
    public void IsSeekerPreparePhase()
    {
        this.GetComponent<PlayerController>().enabled = false;
        SeekerLight.SetActive(false);
        SeekerLightPreparePhase.SetActive(true);
        //HiderLight.SetActive(false);
        //HiderLightPreparePhase.SetActive(false);
    }
    
    [PunRPC]
    public void IsDead()
    {
        GetComponentInChildren<IsTouched>().enabled = false;
        GetComponent<PlayerController>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Rigidbody>().mass = 0;
        GetComponent<MeshRenderer>().enabled = false;
       // HiderLight.SetActive(false);
        //
        this.GetComponentInChildren<FreeFlyCamera>().enabled = true;
    }
    
    // public void CheckHider(Hashtable propertiesThatChanged)
    // {
    //     object Checkplayerhiden;
    //
    //     if (propertiesThatChanged.TryGetValue(PunGameSetting.COUNTHIDENTEAM, out Checkplayerhiden)) {
    //         
    //         //isTimerRunning = true;
    //         CheckHiderAlive = (int)Checkplayerhiden;
    //         Debug.Log("Hiden Player is : " + CheckHiderAlive);
    //     }
    // }

    IEnumerator
        Cooldown()
    {
        yield return new WaitForSeconds(5);
    }
    // private void OnTriggerEnter(Collider other)
    // {
    //     PunUserNetControl yee = this.GetComponent<PunUserNetControl>();
    //     if(yee.MyTeamIs.Name=="Red"&&other.GetComponent<PunUserNetControl>().MyTeamIs.Name == "Blue")
    //     {
    //         
    //     }
    // }
}
