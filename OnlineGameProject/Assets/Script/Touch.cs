﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
public class Touch : MonoBehaviourPunCallbacks
{
    public GameObject touchArea;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!photonView.IsMine)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            photonView.RPC("ActiveTouch",RpcTarget.All);
        }
        else
        {
            photonView.RPC("DeactiveTouch",RpcTarget.All);
        }
    }
    [PunRPC]
    public void ActiveTouch()
    {
        touchArea.SetActive(true);
    }
    
    [PunRPC]
    public void DeactiveTouch()
    {
        touchArea.SetActive(false);
    }
}
